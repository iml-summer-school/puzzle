#include "Puzzle.h"
#include "Reader.h"
#include "State.h"
#include  "Node.h"
#include <fstream>

template<typename T>
static std::ostream& operator<<(std::ostream& out, const std::vector<std::vector<T>>& matrix)
{
	for (auto& line : matrix)
	{
		for (auto& val : line)
		{
			out << val << " ";
		}

		out << std::endl;
	}
	return out;
}

int main()
{
	auto stateMatrix = Reader::readPuzzle();
	State s(stateMatrix);

	if (!s.isSolvable())
	{
		std::cout << s.getMatrix() << "\nis not solvable.\n";
		return 0;
	}

	std::shared_ptr<Node> root = std::make_shared<Node>(s);
	root->setBlank(Node::findBlank(root));

	Puzzle p{root};

	std::cout << p.getRoot()->getState().getMatrix();

	auto goalNode = p.solve();

	std::cout << goalNode->getState().getMatrix();

	std::ofstream out("out.txt");
	auto currentNode = goalNode;
	do {
		out << currentNode->getState().getMatrix();

		out << std::endl;

		currentNode = currentNode->getParent();
	} while (currentNode != nullptr);
	
	
	out << "Success!!\n";

	return 0;
}