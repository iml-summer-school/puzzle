#pragma once
#include <memory>
#include <vector>
#include <array>
#include <utility>
#include "State.h"

class Node
{
public:
	Node(const State& s);

	std::shared_ptr<Node> getParent() const;
	const State& getState() const;

	void setBlank(const std::pair<int, int>&);

	static std::vector<std::shared_ptr<Node>> getSuccessors(std::shared_ptr<Node>);
	static std::pair<int, int> findBlank(std::shared_ptr<Node>);

	bool operator==(const Node& other) const;

	using Hash = State::Hash;

	enum class Directions : char
	{
		NORTH,
		SOUTH,
		WEST,
		EAST,
		Count
	};
	using DirectionsVector = std::array<std::pair<int, int>, static_cast<int>(Directions::Count)>;


private:
	std::shared_ptr<Node> m_parent;

	std::pair<int, int> m_blankPosition;
	State m_state;

	static DirectionsVector m_DirectionsVector;
};
