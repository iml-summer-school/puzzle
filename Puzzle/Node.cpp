#include "Node.h"



Node::Node(const State& s) : m_state{ s }, m_parent{nullptr}
{
}


std::shared_ptr<Node> Node::getParent() const
{
	return m_parent;
}

const State& Node::getState() const
{
	return m_state;
}

void Node::setBlank(const std::pair<int, int>& newBlankPosition)
{
	m_blankPosition = newBlankPosition;
}

std::vector<std::shared_ptr<Node>> Node::getSuccessors(std::shared_ptr<Node> node)
{
	std::vector<std::shared_ptr<Node>> successors;

	for (const auto& directionVector : m_DirectionsVector)
	{
		const auto&[currentBlankRowIdx, currentBlankColIdx] = node->m_blankPosition;

		auto newBlankPosition = std::pair{ currentBlankRowIdx + directionVector.first, currentBlankColIdx + directionVector.second };
		auto&[newBlankRowIdx, newBlankColIdx] = newBlankPosition;
		// is the new blank position a valid position?
		if (newBlankRowIdx >= node->m_state.getMatrix().size() || newBlankColIdx >= node->m_state.getMatrix().size() ||
			newBlankRowIdx < 0 || newBlankColIdx < 0)
		{
			continue;
		}

		auto newNode = successors.emplace_back( std::make_shared<Node>(node->m_state));
		// update blank position
		std::swap(newNode->m_state.getMatrix()[currentBlankRowIdx][currentBlankColIdx],
			newNode->m_state.getMatrix()[newBlankRowIdx][newBlankColIdx]);
		
		newNode->m_blankPosition = newBlankPosition;
		newNode->m_parent = node;
	}

	return successors;
}

std::pair<int, int> Node::findBlank(std::shared_ptr<Node> node)
{
	const auto& matrix = node->getState().getMatrix();
	for (size_t rowIdx = 0; rowIdx < matrix.size(); ++rowIdx)
	{
		for (size_t colIdx = 0; colIdx < matrix.size(); ++colIdx)
		{
			if (matrix[rowIdx][colIdx] == 0)
			{
				return { rowIdx, colIdx };
			}
		}
	}
	throw "Debug -- To implement";
}

bool Node::operator==(const Node & other) const
{
	return this->m_state == other.m_state;
}

Node::DirectionsVector Node::m_DirectionsVector = { std::make_pair<int, int>(-1,  0), { 1,  0 }, { 0, -1 }, { 0,  1 } };