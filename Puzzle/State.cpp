#include "State.h"

State::State(const std::vector<std::vector<int>>& vector) :
	m_state(vector)
{
}

bool State::operator==(const State& other) const
{
	for (int line = 0; line < this->m_state.size(); line++)
	{
		for (int column = 0; column < this->m_state.size(); column++)
		{
			if (this->m_state[line][column] != other.m_state[line][column])
				return false;
		}
	}
	return true;
}

std::vector<std::vector<int>>& State::getMatrix()
{
	return m_state;
}

const std::vector<std::vector<int>>& State::getMatrix() const
{
	return m_state;
}

bool State::isSolvable() const
{
	return (isGridWidthOdd() && isInversionEven()) ||((!isGridWidthOdd() && (( isBlankOnOddRowFromBottom())&&(isInversionEven())))) ;
}

std::vector<std::vector<int>> getGoalMatrix(size_t size)
{
	std::vector<std::vector<int>> matrix(size);

	int n = 1;
	for (size_t rowIdx = 0; rowIdx < matrix.size(); ++rowIdx)
	{
		for (size_t colIdx = 0; colIdx < matrix.size(); ++colIdx)
		{
			matrix[rowIdx].push_back(n++ % (matrix.size() * matrix.size()));
		}
	}
	return matrix;
}

bool State::isGoalState() const
{
	static std::vector<std::vector<int>> goalMatrix = getGoalMatrix(m_state.size());

	return m_state == goalMatrix;
}

bool State::isGridWidthOdd() const
{
	return m_state.size() % 2;
}


bool State::isInversionEven() const
{
	int numberOfInversions = totalNumberOfInversions();
	if (numberOfInversions % 2 == 0)
		return true;
	else return false;
}

bool State::isBlankOnOddRowFromBottom() const
{
	return rowNumberForBlank()% 2;
}

size_t State::totalNumberOfInversions() const
{
	size_t totalInv=0;
	for (const auto& line1 : m_state)
		for (const auto& val1 : line1)
			for (const auto& line2 : m_state)
				for (const auto& val2 : line2)
					if (val1 > val2 && val2 != 0)
						++totalInv;
	return totalInv;
}

size_t State::rowNumberForBlank() const
{
	for (int line = m_state.size()-1; line >= 0; line--)
		for (int column = 0; column < m_state.size(); column++)
			if (m_state[line][column] == 0)
				return line;
}
