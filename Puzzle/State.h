#pragma once
#include "Reader.h"
#include <functional>
#include <iostream>

class State
{
public:
	State(const std::vector<std::vector<int>>& vector);

	bool operator ==(const State& other) const;
	std::vector<std::vector<int>>& getMatrix();
	const std::vector<std::vector<int>>& getMatrix() const;

	using Hash = std::hash<State>;

	bool isSolvable() const;

	bool isGoalState() const;
	
private :
	bool isGridWidthOdd() const;
	bool isInversionEven() const ;
	bool isBlankOnOddRowFromBottom() const;
	size_t totalNumberOfInversions() const;
	size_t rowNumberForBlank() const;

private:
	 std::vector<std::vector<int>> m_state;
};

template<>
struct std::hash<State>
{
	size_t operator() (const State& other)
	{
		auto vec = other.getMatrix();
		std::size_t seed = other.getMatrix().size();
		for (auto& otherVector : vec) {
			for (auto& i:otherVector)
			seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}
		return seed;
	}
};
