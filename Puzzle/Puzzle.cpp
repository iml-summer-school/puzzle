#include "Puzzle.h"
#include <queue>
#include <unordered_set>

Puzzle::Puzzle(std::shared_ptr<Node> root) :
	m_root{ root }
{
}

struct Compare
{
	bool operator()(const std::shared_ptr<Node>& lhs, const std::shared_ptr<Node>& rhs) const
	{
		return *lhs == *rhs;
	}
};

struct Hasher
{
	size_t operator()(const std::shared_ptr<Node>& node) const
	{
		return std::hash<State>()(node->getState());
	}
};

std::shared_ptr<Node> Puzzle::solve() const

{
	std::queue<std::shared_ptr<Node>> fringe;
	std::unordered_set<std::shared_ptr<Node>, Hasher, Compare> closed;

	fringe.push(m_root);
	std::shared_ptr<Node> lastNode = m_root;

	while (!fringe.empty())
	{
		auto currentNode = fringe.front(); fringe.pop();
		lastNode = currentNode;

		if (lastNode->getState().isGoalState()) return lastNode;

		// expand ... currentNode ... // getSuccessors; for each (new state) -> insert into fringe
		for (std::shared_ptr<Node>& successor : Node::getSuccessors(currentNode))
		{

			if (closed.find(successor) == closed.end())
			{
				fringe.emplace(std::move(successor));
			}
		}
		std::cout << ".";
		closed.emplace(std::move(currentNode));
	}

	return lastNode;
}

std::shared_ptr<Node> Puzzle::getRoot() const
{
	return m_root;
}
