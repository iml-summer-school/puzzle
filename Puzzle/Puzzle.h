#pragma once
#include "Node.h"

class Puzzle
{
public:
	Puzzle(std::shared_ptr<Node> root);

	std::shared_ptr<Node> solve() const;

	std::shared_ptr<Node> getRoot() const;
private:
	std::shared_ptr<Node> m_root;
};

